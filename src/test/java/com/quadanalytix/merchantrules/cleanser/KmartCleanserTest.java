package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class KmartCleanserTest extends CleanserTest {
    @Test
    public void testKmartCleanser() throws IOException {
        String cleanedURL1 = getCleanedURL("Kmart.js", "http://www.kmart.com/search=vest&Kmart?filter=storeOrigin&sLevel=0&catalogId=10104&previousSort=ORIGINAL_SORT_ORDER&vDropDown=defaultOpt&viewItems=50&storeId=10151&levels=Workwear+%26+Uniforms_Men%27s+Workwear");
        assertEquals("http://www.kmart.com/search=vest&Kmart?filter=storeOrigin&catalogId=10104&storeId=10151&levels=Workwear+%26+Uniforms_Men%27s+Workwear", cleanedURL1);
        
        cleanedURL1 = getCleanedURL("Kmart.js", "http://www.kmart.com/search=vest&Kmart?filter=storeOrigin&sLevel=0&catalogId=10104&previousSort=ORIGINAL_SORT_ORDER&vDropDown=defaultOpt&viewItems=50&storeId=10151&levels=Workwear+%26+Uniforms_Men%27s+Workwear");
        assertNotEquals("http://www.kmart.com/search=vest&Kmart?filter=storeOrigin&sLevel=0&catalogId=10104&previousSort=ORIGINAL_SORT_ORDER&viewItems=50&storeId=10151&levels=Workwear+%26+Uniforms_Men%27s+Workwear", cleanedURL1);
    	
    	String cleanedURL2 = getCleanedURL("Kmart.js", "http://www.kmart.com/search=spiderman?storeId=10151&catalogId=10104&viewItems=50&levels=Home");
        assertEquals("http://www.kmart.com/search=spiderman?storeId=10151&catalogId=10104&levels=Home", cleanedURL2);
        
        cleanedURL2 = getCleanedURL("Kmart.js", "http://www.kmart.com/search=spiderman?storeId=10151&catalogId=10104&viewItems=50&levels=Home");
        assertNotEquals("http://www.kmart.com/search=spiderman?storeId=10151&catalogId=10104&viewItems=50", cleanedURL2);
    
    	String cleanedURL3 = getCleanedURL("Kmart.js", "http://www.kmart.com/home-bed-bath&WWE/b-1348478556?filter=Brand&previousSort=ORIGINAL_SORT_ORDER&subCatView=true");
        assertEquals("http://www.kmart.com/home-bed-bath&WWE/b-1348478556?filter=Brand&subCatView=true", cleanedURL3);
        
        cleanedURL3 = getCleanedURL("Kmart.js", "http://www.kmart.com/home-bed-bath&WWE/b-1348478556?filter=Brand&previousSort=ORIGINAL_SORT_ORDER&subCatView=true ");
        assertNotEquals("http://www.kmart.com/home-bed-bath&WWE/b-1348478556?filter=Brand&previousSort=ORIGINAL_SORT_ORDER&subCatView=true ", cleanedURL3);
    
    }
}
