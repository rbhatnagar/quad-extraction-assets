package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class ZapposCleanserTest extends CleanserTest {
    @Test
    public void testZapposCleanser() throws IOException {
    
    	String cleanedURL2 = getCleanedURL("Zappos.js", "http://www.zappos.com/belts~1#!/burgundy-belts/COfWARCG1wE6DpsNng3JFZ8NyBWcDY4QQgEHWgkE3Q3EC_gBmgFqAgMC4gIHAQIHCAsNBw.zso?s=isNew/desc/goLiveDate/desc/");
        assertEquals("http://www.zappos.com/belts~1#!/burgundy-belts/COfWARCG1wE6DpsNng3JFZ8NyBWcDY4QQgEHWgkE3Q3EC_gBmgFqAgMC4gIHAQIHCAsNBw.zso", cleanedURL2);
        
        cleanedURL2 = getCleanedURL("Zappos.js", "http://www.zappos.com/belts~1#!/burgundy-belts/COfWARCG1wE6DpsNng3JFZ8NyBWcDY4QQgEHWgkE3Q3EC_gBmgFqAgMC4gIHAQIHCAsNBw.zso?s=isNew/desc/goLiveDate/desc/");
        assertNotEquals("http://www.zappos.com/belts~1#!/burgundy-belts/COfWARCG1wE6DpsNng3JFZ8NyBWcDY4QQgEHWgkE3Q3EC_gBmgFqAgMC4gIHAQIHCAsNBw.zso?s=isNew/desc/goLiveDate/desc/", cleanedURL2);
    }
}
