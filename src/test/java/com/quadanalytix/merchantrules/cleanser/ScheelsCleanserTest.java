package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class ScheelsCleanserTest extends CleanserTest {
    @Test
    public void testScheelsCleanser() throws IOException {
    
    	String cleanedURL = getCleanedURL("Scheels.js", "http://www.scheels.com/shop/SearchDisplay?storeId=10151&catalogId=10051&langId=-1&pageSize=20&beginIndex=0&searchSource=Q&sType=SimpleSearch&orderBy=RELEVANCE&resultCatEntryType=2&showResultsPage=true&pageView=image&searchTerm=pant");
        assertEquals("http://www.scheels.com/shop/SearchDisplay?catalogId=10051&searchSource=Q&sType=SimpleSearch", cleanedURL);
        
        cleanedURL = getCleanedURL("Scheels.js", "http://www.scheels.com/shop/SearchDisplay?storeId=10151&catalogId=10051&langId=-1&pageSize=20&beginIndex=0&searchSource=Q&sType=SimpleSearch&orderBy=RELEVANCE&resultCatEntryType=2&showResultsPage=true&pageView=image&searchTerm=pant");
        assertNotEquals("http://www.scheels.com/shop/SearchDisplay?storeId=10151&catalogId=10051&langId=-1&pageSize=20&beginIndex=0&searchSource=Q&sType=SimpleSearch&orderBy=RELEVANCE&resultCatEntryType=2&showResultsPage=true&pageView=image&searchTerm=pant", cleanedURL);
        
        String cleanedURL2 = getCleanedURL("Scheels.js", "http://www.scheels.com/shop/en/scheels-catalog/search/tents");
        assertEquals("http://www.scheels.com/shop/en/scheels-catalog/search/tents", cleanedURL2);
        
        cleanedURL2 = getCleanedURL("Scheels.js", "http://www.scheels.com/shop/en/scheels-catalog/search/tents");
        assertNotEquals("http://www.scheels.com/shop/en/scheels-catalog/search/tents?", cleanedURL2);
        
        String cleanedURL3 = getCleanedURL("Scheels.js", "http://www.scheels.com/shop/SearchDisplay?categoryId=10090570&sType=SimpleSearch&minPrice=&filterTerm=&metaData=&catalogId=10051&searchTerm=&maxPrice=&manufacturer=");
        assertEquals("http://www.scheels.com/shop/SearchDisplay?categoryId=10090570&sType=SimpleSearch&catalogId=10051", cleanedURL3);
        
        cleanedURL3 = getCleanedURL("Scheels.js", "http://www.scheels.com/shop/SearchDisplay?categoryId=10090570&sType=SimpleSearch&minPrice=&filterTerm=&metaData=&catalogId=10051&searchTerm=&maxPrice=&manufacturer=");
        assertNotEquals("http://www.scheels.com/shop/SearchDisplay?categoryId=10090570&sType=SimpleSearch", cleanedURL3);
    }
}
