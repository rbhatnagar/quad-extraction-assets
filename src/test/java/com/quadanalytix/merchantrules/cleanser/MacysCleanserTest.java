package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class MacysCleanserTest extends CleanserTest {
    @Test
    public void testMacysCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Macys.js", "http://www1.macys.com/shop/kitchen/juicers?id=7583&edge=hybrid");
        assertEquals("http://www1.macys.com/shop/kitchen/juicers?id=7583", cleanedURL);
        
        cleanedURL = getCleanedURL("Macys.js", "http://www1.macys.com/shop/kitchen/utensils-utensil-sets?id=47101&edge=hybrid&cm_sp=us_browse_kitchen-kitchen-kitchen-gadgets-_-row4-_-utensils-%26-utensil-sets");
        assertEquals("http://www1.macys.com/shop/kitchen/utensils-utensil-sets?id=47101", cleanedURL);
    }
}
