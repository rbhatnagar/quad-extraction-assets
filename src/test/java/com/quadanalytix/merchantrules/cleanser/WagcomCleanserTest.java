package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class WagcomCleanserTest extends CleanserTest{
	@Test
    public void testWagCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Wagcom.js", "http://www.wag.com/subcat=Prescription-Dog-Food-303?rf-Type=Kibble");
        assertEquals("http://www.wag.com/subcat=Prescription-Dog-Food-303", cleanedURL);
        
        cleanedURL = getCleanedURL("Wagcom.js", "http://www.wag.com/subcat=Wet-Food-10166?pf_rd_r=001D4B7M6Z0KQ6BB63JH&pf_rd_p=8d2eb781-4d05-4ede-97b4-5ee8ff8c3f8f");
        assertEquals("http://www.wag.com/subcat=Wet-Food-10166", cleanedURL);
        
        cleanedURL = getCleanedURL("Wagcom.js", "http://www.wag.com/subcat=Wet-Food-10166");
        assertEquals("http://www.wag.com/subcat=Wet-Food-10166", cleanedURL);
        
        cleanedURL = getCleanedURL("Wagcom.js", "http://www.wag.com/subcat=Prescription-Dog-Food-303?pg=3");
        assertEquals("http://www.wag.com/subcat=Prescription-Dog-Food-303", cleanedURL);
    }

}
