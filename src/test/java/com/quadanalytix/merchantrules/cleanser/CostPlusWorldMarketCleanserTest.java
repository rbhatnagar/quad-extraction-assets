package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class CostPlusWorldMarketCleanserTest extends CleanserTest {
    @Test
    public void testCostPlusWorldMarketCleanser() throws IOException {
        String cleanedURL = getCleanedURL("CostPlusWorldMarket.js", "http://www.worldmarket.com/category/outdoor/furniture/dining.do?nType=1&ab=third:category:outdoor:furniture:dining");
        assertEquals("http://www.worldmarket.com/category/outdoor/furniture/dining.do", cleanedURL);
        
        cleanedURL = getCleanedURL("CostPlusWorldMarket.js", "http://www.worldmarket.com/category/outdoor/decor/lighting.do");
        assertEquals("http://www.worldmarket.com/category/outdoor/decor/lighting.do", cleanedURL);
    }
}
