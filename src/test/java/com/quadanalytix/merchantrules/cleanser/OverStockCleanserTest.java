package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class OverStockCleanserTest extends CleanserTest {
    @Test
    public void testOverStockCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Overstock.js", "http://www.overstock.com/Home-Garden/Coffee-Makers/43/subcat.html?sort=Top+Sellers");
        assertEquals("http://www.overstock.com/Home-Garden/Coffee-Makers/43/subcat.html", cleanedURL);
        
        cleanedURL = getCleanedURL("Overstock.js", "http://www.overstock.com/Home-Garden/Specialty-Appliances/1988/subcat.html?sort=Top+Sellers");
        assertEquals("http://www.overstock.com/Home-Garden/Specialty-Appliances/1988/subcat.html", cleanedURL);
        
        cleanedURL = getCleanedURL("Overstock.js", "http://www.overstock.com/Home-Garden/Specialty-Appliances/1988/subcat.html");
        assertEquals("http://www.overstock.com/Home-Garden/Specialty-Appliances/1988/subcat.html", cleanedURL);
    }
}
