package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class LululemonCleanserTest extends CleanserTest {
    @Test
    public void testLululemonCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Lululemon.js", "http://shop.lululemon.com/products/category/7-8-pants-run?categoryId=7-8-pants-run&sort=topSellers&filterType1=size&filterValue1=2");
        assertEquals("http://shop.lululemon.com/products/category/7-8-pants-run?categoryId=7-8-pants-run&filterType1=size&filterValue1=2", cleanedURL); 
        cleanedURL = getCleanedURL("Lululemon.js", "http://shop.lululemon.com/products/category/yoga-clothes?lnid=ln;women;features;yoga");
        assertEquals("http://shop.lululemon.com/products/category/yoga-clothes?lnid=ln;women;features;yoga", cleanedURL);
        cleanedURL = getCleanedURL("Lululemon.js", "http://shop.lululemon.com/products/category/mens-tops-to-and-from");
        assertEquals("http://shop.lululemon.com/products/category/mens-tops-to-and-from", cleanedURL);
        
    }
}
