package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class QVCCleanserTest extends CleanserTest {
    @Test
    public void testQVCCleanser() throws IOException {
        String cleanedURL = getCleanedURL("QVC.js", "http://www.qvc.com/electronics/home-office/_/N-1z0f6/c.html?viewType=gallery&pageSize=96&refineOrder=3313122");
        assertEquals("http://www.qvc.com/electronics/home-office/_/N-1z0f6/c.html?refineOrder=3313122", cleanedURL);
        
        cleanedURL = getCleanedURL("QVC.js", "http://www.qvc.com/electronics/home-office/_/N-1z0f6/c.html?viewType=gallery&pageSize=96&refineOrder=3313122");
        assertNotEquals("http://www.qvc.com/electronics/home-office/_/N-1z0f6/c.html", cleanedURL);
        
        cleanedURL = getCleanedURL("QVC.js", "http://www.qvc.com/shoes/shoes/_/N-1cju2/c.html");
        assertEquals("http://www.qvc.com/shoes/shoes/_/N-1cju2/c.html", cleanedURL);
        
        cleanedURL = getCleanedURL("QVC.js", "http://www.qvc.com/shoes/shoes/_/N-1cju2/c.html");
        assertNotEquals("http://www.qvc.com/shoes/shoes/_/N-1cju2/c.html?", cleanedURL);
        
        cleanedURL = getCleanedURL("QVC.js", "http://www.qvc.com/electronics/home-office/_/N-1z0f6/c.html?viewType=gallery&pageSize=96");
        assertNotEquals("http://www.qvc.com/electronics/home-office/_/N-1z0f6/c.html?", cleanedURL);
    }
}
