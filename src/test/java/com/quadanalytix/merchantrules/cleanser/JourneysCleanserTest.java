package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class JourneysCleanserTest extends CleanserTest {

	@Test
    public void testCasaCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("Journeys.js", "http://www.journeys.com/products.aspx?c=833&s=7&g=m&l=gray");
        assertEquals("http://www.journeys.com/products.aspx?c=833", cleanedURL);
        
        cleanedURL = getCleanedURL("Journeys.js", "http://www.journeys.com/kidz/products.aspx?g=w&c=1931");
        assertEquals("http://www.journeys.com/kidz/products.aspx?c=1931", cleanedURL);
        
        cleanedURL = getCleanedURL("Journeys.js", "http://www.journeys.com/kidz/products.aspx?g=m&c=1961");
        assertEquals("http://www.journeys.com/kidz/products.aspx?c=1961", cleanedURL);
        
        cleanedURL = getCleanedURL("Journeys.js", "http://www.journeys.com/kidz/products.aspx?c=901&p=1&g=m&ag=i&s=EACH");
        assertEquals("http://www.journeys.com/kidz/products.aspx?c=901", cleanedURL);
	}
}
