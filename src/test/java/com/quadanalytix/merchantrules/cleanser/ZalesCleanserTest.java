package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class ZalesCleanserTest extends CleanserTest {

	@Test
    public void testZalesCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("Zales.js", "http://www.zales.com/watches/bulova/family.jsp?categoryId=3079641&cp=13337272&ab=MM:Watches:Bulova");
        assertEquals("http://www.zales.com/watches/bulova/family.jsp?categoryId=3079641&ab=MM:Watches:Bulova", cleanedURL);
        
        cleanedURL = getCleanedURL("Zales.js", "http://www.zales.com/bracelets/view-all-bracelets/family.jsp?fd=Gold&categoryId=2109140&ab=MM%3ABracelets%3AGold&fv=Metal+Type%2FGold&cp=13337271&cp=13337271.3263655&fg=Metal+Type&ff=PAD&kpc=1&page=4");
        assertEquals("http://www.zales.com/bracelets/view-all-bracelets/family.jsp?fd=Gold&categoryId=2109140&ab=MM%3ABracelets%3AGold&fv=Metal+Type%2FGold&fg=Metal+Type&ff=PAD", cleanedURL);
        
        cleanedURL = getCleanedURL("Zales.js", "http://www.zales.com/wedding/engagement/family.jsp?categoryId=3045659&fg=Ring+Style&ff=PAD&fv=Ring+Style%2FMulti-Stone&fd=Multi-Stone&cp=13337262");
        assertEquals("http://www.zales.com/wedding/engagement/family.jsp?categoryId=3045659&fg=Ring+Style&ff=PAD&fv=Ring+Style%2FMulti-Stone&fd=Multi-Stone", cleanedURL);
        
        cleanedURL = getCleanedURL("Zales.js", "http://www.zales.com/necklaces/view-all-necklaces/family.jsp?categoryId=2109139&fg=Necklace+Style&ff=PAD&fv=Neck+Style%2FHeart&fd=Heart&cp=13337268&ab=MM:Necklaces:Hearts&cp=13337268.3263553");
        assertEquals("http://www.zales.com/necklaces/view-all-necklaces/family.jsp?categoryId=2109139&fg=Necklace+Style&ff=PAD&fv=Neck+Style%2FHeart&fd=Heart&ab=MM:Necklaces:Hearts", cleanedURL);
        
        cleanedURL = getCleanedURL("Zales.js", "http://www.zales.com/rings/view-all-rings/family.jsp?categoryId=2109136&categoryId=2109136&fg=Price&ff=StorePrice&fv=00050000~-~00074999&fd=%24500.00+-+%24749.00&page=1&size=15&&fbc=1&fbn=StorePrice%7C$500-$749");
        assertEquals("http://www.zales.com/rings/view-all-rings/family.jsp?categoryId=2109136&categoryId=2109136&fg=Price&ff=StorePrice&fv=00050000~-~00074999&fd=%24500.00+-+%24749.00&size=15&&fbc=1&fbn=StorePrice%7C$500-$749", cleanedURL);
	}
}
