package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class NordstromCleanserTest extends CleanserTest {
    @Test
    public void testNordstromCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Nordstrom.js", "http://shop.nordstrom.com/c/mens-pants?dept=8000001&origin=topnav");
        assertEquals("http://shop.nordstrom.com/c/mens-pants", cleanedURL);
        
        cleanedURL = getCleanedURL("Nordstrom.js", "http://shop.nordstrom.com/c/womens-skirts?dept=8000001&origin=topnav");
        assertEquals("http://shop.nordstrom.com/c/womens-skirts", cleanedURL);
        
        cleanedURL = getCleanedURL("Nordstrom.js", "http://shop.nordstrom.com/c/mens-pants?dept=8000001&origin=topnav");
        assertEquals("http://shop.nordstrom.com/c/mens-pants", cleanedURL);
    }
}