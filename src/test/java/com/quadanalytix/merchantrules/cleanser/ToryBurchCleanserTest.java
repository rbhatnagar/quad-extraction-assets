package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class ToryBurchCleanserTest extends CleanserTest{
	@Test
    public void testToryBurchCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("ToryBurch.js", "http://www.toryburch.com/shoes-newarrivals/#?undefined&format=ajax");
        assertEquals("http://www.toryburch.com/shoes-newarrivals/", cleanedURL);
        
        cleanedURL = getCleanedURL("ToryBurch.js", "http://www.toryburch.com/shoes/flip-flops/#?prefn1=color&prefn2=silhouettetype&prefv3=10&prefv1=Green|Red&prefv2=shoe-flat-flip-flop%20%28Flat%29&prefn3=size");
        assertEquals("http://www.toryburch.com/shoes/flip-flops/", cleanedURL);
        
        cleanedURL = getCleanedURL("ToryBurch.js", "http://www.toryburch.com/shoes-newarrivals/#?prefn1=Category&prefv1=shoe-booties|shoes-sandals");
        assertEquals("http://www.toryburch.com/shoes-newarrivals/", cleanedURL);
        
        cleanedURL = getCleanedURL("ToryBurch.js", "http://www.toryburch.com/clothing/tops/");
        assertEquals("http://www.toryburch.com/clothing/tops/", cleanedURL);
        
	}
}
