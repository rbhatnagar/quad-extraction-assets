package com.quadanalytix.merchantrules.cleanser;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import org.apache.commons.io.IOUtils;

import com.quadanalytix.extractionservice.jsfunctions.JSFunction;

public class CleanserTest {
    String getCleanedURL(final String fileName, final String url) throws IOException {
        try (InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("cleansers/" + fileName)) {
            final String script = IOUtils.toString(input, "UTF-8");
            final JSFunction deduper = new JSFunction(Arrays.asList("url"), script);
            final Object value = deduper.apply(new Object[] { url });
            return value.toString();    
        }
    }
}
