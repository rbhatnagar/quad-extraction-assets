package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class RestorationHardwareTest extends CleanserTest {
    @Test
    public void testRestorationHardwareCleanser() throws IOException {
        String cleanedURL = getCleanedURL("RestorationHardware.js", "http://www.restorationhardware.com/catalog/product/product.jsp?productId=prod2780025&categoryId=cat3870041");
        assertEquals("http://www.restorationhardware.com/catalog/product/product.jsp?productId=prod2780025", cleanedURL);
        
        cleanedURL = getCleanedURL("RestorationHardware.js", "http://www.restorationhardware.com/catalog/category/products.jsp?categoryId=cat2110006");
        assertEquals("http://www.restorationhardware.com/catalog/category/products.jsp?categoryId=cat2110006", cleanedURL);
    }
}
