package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class JCPennyCleanserTest extends CleanserTest {
    @Test
    public void testJCPenneyCleanser() throws IOException {
        String cleanedURL = getCleanedURL("JCPenney.js", "http://www.jcpenney.com/window/curtain-rods-hardware/cat.jump?id=cat100260231&deptId=dept20022800026&cmJCP_T=G1&cmJCP_C=D3");
        assertEquals("http://www.jcpenney.com/cat.jump?id=cat100260231", cleanedURL);
        
        cleanedURL = getCleanedURL("JCPenney.js", "http://www.jcpenney.com/royal-velvet-400tc-wrinkleguard-duvet-cover-accessories/prod.jump?ppId=cat1a6ddbd&catId=cat100290025&deptId=dept20000012&&_dyncharset=UTF-8&colorizedImg=0900631B81C2F8A0M.tif");
        assertEquals("http://www.jcpenney.com/cat.jump?id=cat1a6ddbd", cleanedURL);
    }
}
