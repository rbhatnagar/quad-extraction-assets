package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class GrouponGoodsCleanserTest extends CleanserTest{
	
	@Test
    public void testGrouponGoodsCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("GrouponGoods.js", "https://www.groupon.com/goods/wallets");
        assertEquals("https://www.groupon.com/goods/wallets", cleanedURL);
        
        cleanedURL = getCleanedURL("GrouponGoods.js", "https://www.groupon.com/goods/wallets?brand=886a46ed-70c1-47e4-a3af-5d6ce7bfbf59");
        assertEquals("https://www.groupon.com/goods/wallets", cleanedURL);
        
        cleanedURL = getCleanedURL("GrouponGoods.js", "https://www.groupon.com/goods/coats-and-jackets?maxPrice=442&minPrice=9");
        assertEquals("https://www.groupon.com/goods/coats-and-jackets", cleanedURL);
        
        cleanedURL = getCleanedURL("GrouponGoods.js", "https://www.groupon.com/goods/coats-and-jackets?discount=gg-clearance");
        assertEquals("https://www.groupon.com/goods/coats-and-jackets", cleanedURL);
        
	}

}
