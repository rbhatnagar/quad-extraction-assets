package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class BestBuyCleanserTest extends CleanserTest {
    @Test
    public void testBestBuyCleanser() throws IOException {
        String cleanedURL = getCleanedURL("BestBuy.js", "http://www.bestbuy.com/site/olstemplatemapper.jsp?_dyncharset=UTF-8&_dynSessConf=7175430542304825317&id=pcat17071&type=page&ks=960&st=categoryid%24abcat0912016&sc=Global&cp=1&sp=-bestsellingsort+skuidsaas&qp=soldby_facet%3DSAAS~Sold+By~Best+Buy&list=y&usc=All+Categories&nrp=15&fs=saas&iht=n&seeAll=&browsedCategory=abcat0912016");
        assertEquals("http://www.bestbuy.com/site/olstemplatemapper.jsp?id=pcat17071&st=categoryid%24abcat0912016&sp=-bestsellingsort+skuidsaas&qp=soldby_facet%3DSAAS~Sold+By~Best+Buy&list=y&usc=All+Categories&nrp=15&fs=saas&seeAll=&browsedCategory=abcat0912016", cleanedURL); 
        cleanedURL = getCleanedURL("BestBuy.js", "http://www.bestbuy.com/site/toaster-ovens-pizza-ovens/toaster-ovens/pcmcat334200050020.c?id=pcmcat334200050020");
        assertEquals("http://www.bestbuy.com/site/toaster-ovens-pizza-ovens/toaster-ovens/pcmcat334200050020.c?id=pcmcat334200050020", cleanedURL); 
        
    }
}
