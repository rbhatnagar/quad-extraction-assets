package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class BedBathAndBeyondCleanserTest extends CleanserTest {
    @Test
    public void testBedBathAndBeyondCleanser() throws IOException {
        String cleanedURL = getCleanedURL("BedBathandBeyond.js", "http://www.bedbathandbeyond.com/store/category/tea-kettles-pots/12117/?a=1&CatalogId=12117+4294966331+516311261&CatalogRefId=12117");
        assertEquals("http://www.bedbathandbeyond.com/store/category/tea-kettles-pots/12117/", cleanedURL);
        
        cleanedURL = getCleanedURL("BedBathandBeyond.js", "http://www.bedbathandbeyond.com/store/category/espresso-machines/12054/");
        assertEquals("http://www.bedbathandbeyond.com/store/category/espresso-machines/12054/", cleanedURL);
    }
}
