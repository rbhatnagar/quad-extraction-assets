package com.quadanalytix.merchantrules.cleanser;
import static org.junit.Assert.assertEquals;
import java.io.IOException;
import org.junit.Test;

public class TheLandofNodCleanserTest extends CleanserTest {
    @Test
    public void testTheLandofNodCleanser() throws IOException {
        String cleanedURL = getCleanedURL("TheLandofNod.js", "http://www.landofnod.com/shelves/storage/1");
        assertEquals("http://www.landofnod.com/shelves/storage", cleanedURL);
        
        cleanedURL = getCleanedURL("TheLandofNod.js", "http://www.landofnod.com/shelves/storage/");
        assertEquals("http://www.landofnod.com/shelves/storage", cleanedURL);
        
        cleanedURL = getCleanedURL("TheLandofNod.js", "http://www.landofnod.com/shelves/storage");
        assertEquals("http://www.landofnod.com/shelves/storage", cleanedURL);
        
        cleanedURL = getCleanedURL("TheLandofNod.js", "http://www.landofnod.com/little-study-drawers/s196606?rv=Storage%3ashelves%3aStraight+%26+Narrow+Wall+Rack+%28White%29+Family+Page");
        assertEquals("http://www.landofnod.com/little-study-drawers/s196606", cleanedURL);
    }
}