package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class LandsEndCleanserTest extends CleanserTest {
    @Test
    public void testLandsEndCleanser() throws IOException {
        String cleanedURL = getCleanedURL("LandsEnd.js", "http://www.landsend.com/shop/mens-coats/-/N-fz7?cm_re=nav-_-outerwear-_-catagories-_-men");
        assertEquals("http://www.landsend.com/shop/mens-coats/-/N-fz7", cleanedURL);
        
        cleanedURL = getCleanedURL("LandsEnd.js", "http://www.landsend.com/shop/mens-dress-shirts/-/N-fzl?cm_re=nav-_-men-_-catagories-_-dress-shirt");
        assertEquals("http://www.landsend.com/shop/mens-dress-shirts/-/N-fzl", cleanedURL);
    }
}
