package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;


public class BloomingDalesCleanserTest extends CleanserTest {
    @Test
    public void testBloomingDalesCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Bloomingdales.js", "http://www1.bloomingdales.com/shop/all-designers/handbags?id=1001340&cm_sp=NAVIGATION_INTL-_-TOP_NAV-_-1001351-BY-CATEGORY-Handbags");
        assertEquals("http://www1.bloomingdales.com/shop/all-designers/handbags?id=1001340", cleanedURL);
        
        cleanedURL = getCleanedURL("Bloomingdales.js", "http://www1.bloomingdales.com/shop/jewelry-accessories/tech-accessories-cases?id=21485&cm_sp=NAVIGATION_INTL-_-TOP_NAV-_-3376-ACCESSORIES-Tech-Accessories-%26-Cases");
        assertEquals("http://www1.bloomingdales.com/shop/jewelry-accessories/tech-accessories-cases?id=21485", cleanedURL);
    }
}
