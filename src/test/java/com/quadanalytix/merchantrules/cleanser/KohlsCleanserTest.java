package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class KohlsCleanserTest extends CleanserTest {
    @Test
    public void testKohlsCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Kohls.js", "http://www.kohls.com/catalog/bedspreads-bedding-bed-and-bath.jsp?CN=4294719683+4294719802+4294719803&N=4294719803+4294719802+4294717606+4294719683");
        assertEquals("http://www.kohls.com/catalog/bedspreads-bedding-bed-and-bath.jsp?N=4294719803+4294719802+4294717606+4294719683", cleanedURL);
        
        cleanedURL = getCleanedURL("Kohls.js", "http://www.kohls.com/catalog/womens-bootcut-jeans-bottoms-clothing.jsp?CN=4294720878+4294737278+4294719454+4294719807+4294719810&cc=jeanshop-spot1-bootcut");
        assertEquals("http://www.kohls.com/catalog/womens-bootcut-jeans-bottoms-clothing.jsp?CN=4294720878+4294737278+4294719454+4294719807+4294719810", cleanedURL);
        
        cleanedURL = getCleanedURL("Kohls.js", "http://www.kohls.com/catalog/bedspreads-bedding-bed-and-bath.jsp?CN=4294719683+4294719802+4294719803&N=4294719803+4294719802+4294717606+4294719683");
        assertEquals("http://www.kohls.com/catalog/bedspreads-bedding-bed-and-bath.jsp?N=4294719803+4294719802+4294717606+4294719683", cleanedURL);
    }
}