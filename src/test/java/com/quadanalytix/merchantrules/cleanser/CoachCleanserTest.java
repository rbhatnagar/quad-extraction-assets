package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class CoachCleanserTest extends CleanserTest {

	@Test
    public void testCoachCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("Coach.js", "http://www.coach.com/shop/men-bags?viewAll=true");
        assertEquals("http://www.coach.com/shop/men-bags", cleanedURL);
        
        cleanedURL = getCleanedURL("Coach.js", "http://www.coach.com/shop/women-handbags-new-arrivals");
        assertEquals("http://www.coach.com/shop/women-handbags-new-arrivals", cleanedURL);
        
	}
}
