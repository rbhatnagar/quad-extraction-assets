package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class TargetCleanserTest extends CleanserTest {
    
    @Test
    public void testTargetCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Target.js", "http://www.target.com/c/tea-pots-kettles-coffee-espresso-kitchen-appliances/-/N-5xtrm#?lnk=lnav_shop categories_4");
        assertEquals("http://www.target.com/c/tea-pots-kettles-coffee-espresso-kitchen-appliances/-/N-5xtrm", cleanedURL);
        
        cleanedURL = getCleanedURL("Target.js", "http://www.target.com/c/warmers-buffets-cooktops-kitchen-appliances-dining/-/N-5xtrd#?lnk=lnav_shop categories_10");
        assertEquals("http://www.target.com/c/warmers-buffets-cooktops-kitchen-appliances-dining/-/N-5xtrd", cleanedURL);
    }
}
