package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class WayfairCleanserTest extends CleanserTest {
    @Test
    public void wayfairCleanserTest() throws IOException {
        String cleanedURL = getCleanedURL("Wayfair.js", "http://www.wayfair.com/sb2/Steak-Knives-l916-c419543-O7213~Steak+Knife-O7214~Stainless+Steel.html?itemsperpage=48&sortby=66");
        assertEquals("http://www.wayfair.com/sb2/Steak-Knives-l916-c419543-O7213~Steak+Knife-O7214~Stainless+Steel.html", cleanedURL);
        
        cleanedURL = getCleanedURL("Wayfair.js", "http://www.wayfair.com/sb2/Steak-Knives-l916-c419543-O7213~Steak+Knife-O7214~Stainless+Steel.html?itemsperpage=48&sortby=66");
        assertNotEquals("http://www.wayfair.com/sb", cleanedURL);
        
    }
}
