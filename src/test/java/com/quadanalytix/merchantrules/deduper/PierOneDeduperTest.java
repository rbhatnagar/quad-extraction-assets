package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class PierOneDeduperTest extends DeduperTest {
    @Test
    public void testPierOneDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("PierOne.js", "http://www.pier1.com/Turquesa-Tile-Duvet-Cover-Sham/PS48720,default,pd.html?cgid=patterned-bedding#nav=top&start=1");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("PS48720", uniquesAttrs.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("PierOne.js", "http://www.pier1.com/Dove-Savannah-Duvet-Cover---Full/Queen/2726889,default,pd.html");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("2726889", uniquesAttrs2.get(0));
    }
}
