package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class HomeShoppingNetworkDeduperTest extends DeduperTest {
    @Test
    //Positive Test
    public void testHomeShoppingNetworkDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("HomeShoppingNetwork.js", "http://www.hsn.com/products/vince-camuto-jaran-wide-calf-tall-leather-boot/7802796");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("7802796", uniquesAttrs.get(0));
    }
    
    @Test
    //Negative Test
    public void test2HomeShoppingNetworkDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("HomeShoppingNetwork.js", "http://www.google.com");
        assertTrue(uniquesAttrs.size()==0);
     }
}
