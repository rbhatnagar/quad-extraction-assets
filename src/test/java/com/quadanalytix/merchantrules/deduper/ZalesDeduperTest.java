package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class ZalesDeduperTest extends DeduperTest {
	@Test
    public void testZalesDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Zales.js", "http://www.zales.com/unstoppable-love-diamond-square-frame-drop-earrings-10k-white-gold/product.jsp?productId=31911776&ab=MM%3ACollections%3AUnstoppable+Love");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("31911776", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Zales.js", "http://www.zales.com/diamond-square-composite-frame-ring-10k-gold/product.jsp?productId=38930456");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("38930456", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Zales.js", "http://www.zales.com/mothers-sideways-hearts-synthetic-birthstone-ring-sterling-silver-stones/product.jsp?productId=25112336&ab=PERSONALIZEDLP%3AMOTHERFAMILY");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("25112336", uniquesAttrs3.get(0));
        
        final List<String> uniquesAttrs4 = getUniqueAttributes("Zales.js", "http://www.zales.com/lab-created-white-sapphire-heart-locket-pendant-sterling-silver/product.jsp?productId=13358443&ab=MM%3ANecklaces%3AGemstone");
        assertTrue(uniquesAttrs4 != null);
        assertEquals(1, uniquesAttrs4.size());
        assertEquals("13358443", uniquesAttrs4.get(0));
        
    }
}
