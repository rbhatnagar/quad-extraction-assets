package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class AthletaDeduperTest extends DeduperTest {
    @Test
    public void testAthletaDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Athleta.js", "http://athleta.gap.com/browse/product.do?cid=1005761&vid=1&pid=724614002");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("724614002", uniquesAttrs.get(0));
    }
}
