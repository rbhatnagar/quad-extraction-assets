package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class WalmartDeduperTest extends DeduperTest {
    @Test
    public void testWalmartDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Walmart.js", "http://www.walmart.com/ip/33492307?findingMethod=mhl");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("33492307", uniquesAttrs.get(0));
    }
}
