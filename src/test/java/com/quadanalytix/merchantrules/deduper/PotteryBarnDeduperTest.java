package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class PotteryBarnDeduperTest extends DeduperTest {
    @Test
    public void testPotteryBarnDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("PotteryBarn.js", "http://www.potterybarn.com/products/pb-basic-sleeper-sofa/?pkey=e%7Csleeper%2Bsofas%7C16%7Cbest%7C0%7C1%7C24%7C%7C6&cm_src=PRODUCTSEARCH||NoFacet-_-NoFacet-_-NoMerchRules");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("pb-basic-sleeper-sofa", uniquesAttrs.get(0));
    }
}
