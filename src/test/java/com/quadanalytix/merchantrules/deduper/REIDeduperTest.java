package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class REIDeduperTest extends DeduperTest {
    @Test
    public void testReiDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("REI.js", "http://www.rei.com/product/872540/five-ten-maltese-falcon-bike-shoes-mens");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("872540", uniquesAttrs.get(0));
    }
}
