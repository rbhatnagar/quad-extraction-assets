package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class EddieBauerDeduperTest extends DeduperTest {
    @Test
    public void testEddieBauerDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("EddieBauer.js", "http://www.eddiebauer.com/product/wrinkle-free-relaxed-fit-comfort-waist-flat-front-casual-performance-chino-pants/10307505/_/A-ebSku_0035433257001034__10307505_catalog10002_en__US?showProducts=&backToCat=Pants&previousPage=LNAV&tab=men&color=257");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("10307505", uniquesAttrs.get(0));
    }
}
