package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class BestBuyDeduperTest extends DeduperTest {
    @Test
    public void testBestBuyDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("BestBuy.js", "http://www.bestbuy.com/site/GoPro+-+HERO3+HD+Camcorder+-+Black/6571689.p?id=1218757813679&skuId=6571689");
        assertTrue(uniquesAttrs != null);
        assertEquals(2, uniquesAttrs.size());
        assertEquals("1218757813679", uniquesAttrs.get(0));
        assertEquals("6571689", uniquesAttrs.get(1));
    }
}
