package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class BloomingDalesDeduperTest extends DeduperTest {
    @Test
    public void testBloomingDalesDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Bloomingdales.js", "http://www1.bloomingdales.com/shop/product/woolrich-john-rich-bros-coat-literary-walk?ID=1060643&CategoryID=1001520#fn=spp%3D1%26ppp%3D96%26sp%3D1%26rid%3D%26spc%3D620");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("1060643", uniquesAttrs.get(0));
    }
}
