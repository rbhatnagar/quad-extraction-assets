package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class ZumiezDeduperTest extends DeduperTest {
    @Test
    public void testZumiezDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Zumiez.js", "http://www.zumiez.com/empyre-insignia-blackberry-colorblock-tech-fleece-jacket.html");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("empyre-insignia-blackberry-colorblock-tech-fleece-jacket", uniquesAttrs.get(0));
    }
}
