package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class CoachDeduperTest extends DeduperTest {
	@Test
    public void testCoachDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Coach.js", "http://www.coach.com/coach-designer-handbags-ace-satchel-in-glovetanned-leather/37017.html?dwvar_color=LHBUR&cgid=women-handbags-new-arrivals");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("37017", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Coach.js", "http://www.coach.com/coach-mens-travel-explorer-bag-52-in-pebble-leather/71664.html?dwvar_color=QB%2FBK&cgid=men-bags");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("71664", uniquesAttrs2.get(0));
    }

}
