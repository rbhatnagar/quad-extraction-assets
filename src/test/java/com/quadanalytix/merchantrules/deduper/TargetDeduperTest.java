package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class TargetDeduperTest extends DeduperTest {
    @Test
    public void testTargetDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Target.js", "http://www.target.com/p/sterilite-2-shelf-storage-cabinet/-/A-11861497#prodSlot=medium_1_4");
        assertTrue(uniquesAttrs != null);
        assertEquals(2, uniquesAttrs.size());
        assertEquals("sterilite-2-shelf-storage-cabinet", uniquesAttrs.get(0));
        assertEquals("A-11861497", uniquesAttrs.get(1));
    }
}
