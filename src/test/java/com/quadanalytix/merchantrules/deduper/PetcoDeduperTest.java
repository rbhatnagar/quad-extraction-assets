package com.quadanalytix.merchantrules.deduper;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class PetcoDeduperTest extends DeduperTest {
	@Test
    public void testPetcoDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Petco.js", "http://www.petco.com/shop/en/petcostore/bird/bird-cages-and-accessories/aande-cage-company-grey-palace-dometop-x-large-bird-cage-5003861--1");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("aande-cage-company-grey-palace-dometop-x-large-bird-cage-5003861--1", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Petco.js", "http://www.petco.com/shop/en/petcostore/reptile/reptile-habitats-and-enclosures/exo-terra-bent-glass-turtle-terrarium");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("exo-terra-bent-glass-turtle-terrarium", uniquesAttrs2.get(0));
    }
}
