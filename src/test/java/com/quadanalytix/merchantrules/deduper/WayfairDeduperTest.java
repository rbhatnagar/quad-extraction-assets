package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class WayfairDeduperTest extends DeduperTest {
    @Test
    public void testWayfairDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Wayfair.js", "http://www.wayfair.com/Bauhaus-Bath-Jacchi-28-Single-Bathroom-Vanity-Set-Jacchi28C-BWW1011.html");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("BWW1011", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs1 = getUniqueAttributes("Wayfair.js", "http://www.wayfair.com/daily-sales/p/Zipcode-Design-4010009~ZIPC1088~E19658.html");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("ZIPC1088", uniquesAttrs1.get(0));
    }
}
