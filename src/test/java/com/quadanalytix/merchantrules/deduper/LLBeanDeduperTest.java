package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class LLBeanDeduperTest extends DeduperTest {
    @Test
    public void testLLBeanDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("LLBean.js", "http://www.llbean.com/llb/shop/68897?feat=513283-GN1&page=easy-stretch-pants-denim");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("easy-stretch-pants-denim", uniquesAttrs.get(0));
    }
}
