package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class PatagoniaDeduperTest extends DeduperTest {
    @Test
    public void testPatagoniaDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Patagonia.js", "http://www.patagonia.com/us/product/mens-special-edition-nano-puff-pullover?p=84520-0");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("84520", uniquesAttrs.get(0));
    }
}
