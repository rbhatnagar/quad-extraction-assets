package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class CostPlusWorldMarketDeduperTest extends DeduperTest {

    @Test
    public void testChefsCatalogDeduper() throws IOException, URISyntaxException {
        List<String> uniquesAttrs = getUniqueAttributes("CostPlusWorldMarket.js", "http://www.worldmarket.com/product/beach-stripe-paper-string-lights.do?&from=fn");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("beach-stripe-paper-string-lights", uniquesAttrs.get(0));
        
        uniquesAttrs = getUniqueAttributes("CostPlusWorldMarket.js", "http://www.worldmarket.com/product/edison-style+string+lights.do?fMb=1&green=7AEBA925-1AD5-5B6E-9C1E-A0FBE2D80CC3&Clickid=mybuys_prod_cs");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("edison-style+string+lights", uniquesAttrs.get(0));
    }
    
}
