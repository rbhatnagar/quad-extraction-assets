package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class GrouponGoodsDeduperTest extends DeduperTest{
	
	@Test
    public void testGrouponGoodsDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("GrouponGoods.js", "https://www.groupon.com/deals/gg-printer-ink-and-toner-1");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("gg-printer-ink-and-toner-1", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("GrouponGoods.js", "https://www.groupon.com/deals/gg-glass-bowl-sets/");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("gg-glass-bowl-sets", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("GrouponGoods.js", "https://www.groupon.com/deals/gg-anti-snore-wristband-32");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("gg-anti-snore-wristband-32", uniquesAttrs3.get(0));
    }
}
