package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class LululemonDeduperTest extends DeduperTest {
    @Test
    public void testLululemonDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Lululemon.js", "http://shop.lululemon.com/products/clothes-accessories/pants-yoga/Dance-To-Yoga-Pant?cc=18867&skuId=3616894&catId=pants-yoga");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("Dance-To-Yoga-Pant", uniquesAttrs.get(0));
    }
}
