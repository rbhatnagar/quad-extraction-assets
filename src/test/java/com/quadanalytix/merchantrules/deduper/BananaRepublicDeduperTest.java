package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class BananaRepublicDeduperTest extends DeduperTest {
    @Test
    public void testBananaRepublicDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("BananaRepublic.js", "http://bananarepublic.gap.com/browse/product.do?cid=1007624&vid=1&pid=969860002");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("969860002", uniquesAttrs.get(0));
    }
}
