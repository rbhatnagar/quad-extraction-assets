package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class BackcountryDeduperTest extends DeduperTest {
	@Test
    public void testBackcountryDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Backcountry.js", "http://www.backcountry.com/arcteryx-dually-belay-insulated-parka-mens?ti=UExQIEJyYW5kOk1lbidzIEphY2tldHM6MTozMDoxODdfYmNzQ2F0MTEwMDA0&skid=ARC3681-BK-S");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("arcteryx-dually-belay-insulated-parka-mens", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Backcountry.js", "http://www.backcountry.com/olukai-kaiulani-boot-womens?ti=UExQIFJ1bGUgQmFzZWQ6V29tZW4ncyBGb290d2VhciBCb290czoxOjMzOmNhdDEwMDIwNzI2Mg&skid=OLU000S-SEABRO-S7");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("olukai-kaiulani-boot-womens", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Backcountry.js", "http://www.backcountry.com/oakley-gearbox-backpack-1953cu-in");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("oakley-gearbox-backpack-1953cu-in", uniquesAttrs3.get(0));
    }

}