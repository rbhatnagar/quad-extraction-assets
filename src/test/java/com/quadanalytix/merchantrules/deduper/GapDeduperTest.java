package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class GapDeduperTest extends DeduperTest {
  @Test
  public void testEddieBauerDeduper() throws IOException, URISyntaxException {
      final List<String> uniquesAttrs = getUniqueAttributes("Gap.js", "http://www.gap.com/browse/product.do?cid=1035682&vid=1&pid=421545002");
      assertTrue(uniquesAttrs != null);
      assertEquals(1, uniquesAttrs.size());
      assertEquals("421545002", uniquesAttrs.get(0));
  }
  
  @Test
  public void testEddieBauerDeduper2() throws IOException, URISyntaxException {
      final List<String> uniquesAttrs = getUniqueAttributes("Gap.js", "http://www.google.com");
      assertTrue(uniquesAttrs.isEmpty());
      
     
  }
}
