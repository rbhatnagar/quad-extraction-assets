package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class BedBathAndBeyondTDeduperTest extends DeduperTest {
    @Test
    public void testBedBathAndBeyondDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("BedBathandBeyond.js", "http://www.bedbathandbeyond.com/store/product/Keurig-B60-K65-Special-Edition-Brewer/118327?categoryId=12053");
        assertTrue(uniquesAttrs != null);
        assertEquals(2, uniquesAttrs.size());
        assertEquals("Keurig-B60-K65-Special-Edition-Brewer", uniquesAttrs.get(0));
        assertEquals("118327", uniquesAttrs.get(1));
    }
}
