package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class OldNavyDeduperTest extends DeduperTest {
    @Test
    public void testOldNavyDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("OldNavy.js", "http://oldnavy.gap.com/browse/product.do?cid=99279&vid=3&pid=209501012");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("209501012", uniquesAttrs.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("OldNavy.js", "http://oldnavy.gap.com/browse/product.do?cid=99541&vid=1&pid=148185012");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("148185012", uniquesAttrs2.get(0));
    }
}
