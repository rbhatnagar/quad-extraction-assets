package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class JCrewFactoryDeduperTest extends DeduperTest {
    @Test
    public void testJCrewDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("JCrewFactory.js", "https://factory.jcrew.com/girls-clothing/shirts_tops/shirts/PRDOVR~C9802/C9802.jsp");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("C9802", uniquesAttrs.get(0));
    }
}
