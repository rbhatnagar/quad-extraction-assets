package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class VonMaurDeduperTest extends DeduperTest {
    @Test
    public void testVonMaurDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("VonMaur.js", "http://vonmaur.com/Product.aspx?ID=1283539&Sale=n&pg=1");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("1283539", uniquesAttrs.get(0));
    }
    
    @Test
    public void testVonMaurDeduper2() throws IOException, URISyntaxException {
      final List<String> uniquesAttrs = getUniqueAttributes("VonMaur.js", "http://vonmaur.com/Product.aspx?ID=1271351&Sale=y&pg=1");
      assertTrue(uniquesAttrs != null);
      assertEquals(1, uniquesAttrs.size());
      assertEquals("1271351", uniquesAttrs.get(0));
  }
}
