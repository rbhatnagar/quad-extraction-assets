package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class ChefsDeduperTest extends DeduperTest {
    @Test
    public void testChefsCatalogDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("CHEFS.js", "http://www.chefscatalog.com/product/26527-curious-chef-27-piece-foundation-set.aspx");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("26527", uniquesAttrs.get(0));
    }
}
