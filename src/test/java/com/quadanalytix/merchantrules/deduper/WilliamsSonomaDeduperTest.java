package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class WilliamsSonomaDeduperTest extends DeduperTest {
    @Test
    public void testWilliamsSonomaDeduper() throws IOException, URISyntaxException {
        List<String> uniquesAttrs = getUniqueAttributes("WilliamsSonoma.js", "http://www.williams-sonoma.com/products/technivorm-moccamaster-coffee-maker-thermal-carafe-silver/?pkey=ccoffee-makers&cm_src=coffee-makers||NoFacet-_-NoFacet-_--_-");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("technivorm-moccamaster-coffee-maker-thermal-carafe-silver", uniquesAttrs.get(0));
        
        uniquesAttrs = getUniqueAttributes("WilliamsSonoma.js", "http://www.williams-sonoma.com/products/bonjour-maximus-milk-frother/?pkey=cespresso-makers&cm_src=espresso-makers||NoFacet-_-NoFacet-_--_-");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("bonjour-maximus-milk-frother", uniquesAttrs.get(0));      
    }
}
