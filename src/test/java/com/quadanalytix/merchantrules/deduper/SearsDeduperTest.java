package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class SearsDeduperTest extends DeduperTest {
    @Test
    public void testSearsDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Sears.js", "http://www.sears.com/presto-12-cup-coffee-maker/p-00809795000P?prdNo=22&blockNo=22&blockType=G22");
        assertTrue(uniquesAttrs != null);
        assertEquals(2, uniquesAttrs.size());
        assertEquals("presto-12-cup-coffee-maker", uniquesAttrs.get(0));
        assertEquals("00809795000P", uniquesAttrs.get(1));
    }
}
