package com.quadanalytix.extractionservice.jsfunctions;

import java.util.List;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.ContextFactory;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.ScriptableObject;

/**
 * Wrapper class over the rhino javascript function object.
 * 
 * @author abhishek
 */
public class JSFunction {
    private List<String> mInputs;
    private String mScript;
    private Function mFunction;
    private ContextFactory mContextFactory;
    private ScriptableObject mScope;
    
    /**
     * Constructor taking functions inputs and script as argument.
     * @param inputs
     * @param script
     */
    public JSFunction(final List<String> inputs, final String script) {
        mInputs = inputs;
        mScript = script;
        intialize();
    }

    private void intialize() {
        mContextFactory = ContextFactory.getGlobal();
        final Context context = mContextFactory.enterContext();
        context.setOptimizationLevel(9);
        mScope = context.initStandardObjects();
        mFunction = context.compileFunction(mScope, mScript, "deduper", 1, null);
        Context.exit();
    }

    /**
     * Executes the function with passed in values as inputs to the function.
     * @param values
     * @return output of the function
     */
    public Object apply(final Object[] values) {
        if (values == null || (values.length < mInputs.size()) || mFunction == null) {
            return null;
        }
        try {
            Context cx = Context.getCurrentContext();
            if (cx == null) {
                cx = mContextFactory.enterContext();
            }
            final Object[] convertedValues = new Object[values.length];
            for (int i = 0; i < values.length; i++) {
                final Object value = values[i] == null ? null : values[i];
                final Object javaToJS = Context.javaToJS(value, mScope);
                convertedValues[i] = javaToJS;
                ScriptableObject.putProperty(mScope, mInputs.get(i), javaToJS);
            }
            return mFunction.call(cx, mScope, mScope, convertedValues);
        } catch (final Throwable th) {
            return null;
        }
    }
}