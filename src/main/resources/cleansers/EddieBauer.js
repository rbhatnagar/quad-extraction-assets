function filterURL(url){
    var required_elem = ["Ntt"]; //array of elements needed to be there in url
    var final_query_string_elems = []; //array of those query string variables that were required and were found in url and are kept further
    
	if(url.indexOf("?") > -1)
	{
        var query_string_elem = null;
		var mainUrl = url.split("?");
		var res =  mainUrl[1].split("&");
		
		for (var i in res) {
            query_string_elem = res[i].split("=")[0];//element from url
            
            //if this element is in required_elem and is not already pushed before to final_query_string_elems
        	if(required_elem.indexOf(query_string_elem) != -1 && final_query_string_elems.indexOf(res[i]) == -1)
        	{
                final_query_string_elems.push(res[i]);
        	}
        	
    	}
        if(final_query_string_elems.length > 0)
    	{
    		final_query_string_elems[0] = "?" + final_query_string_elems[0];//prefix 1st final_query_string_variable with ?
    	}
        url = mainUrl[0] + final_query_string_elems.join("&");
	}
	return url;
}