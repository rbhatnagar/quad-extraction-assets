function clean(url) {
    var required_params = []
	var toReturn = url;
	// remove extra params like page_number 
	var params_to_clean = ['cp', 'kpc', 'page'];
	var index = url.indexOf("?");
	if(index >= 0){
	    var url_n_params = url.split('?');
	    var base_url = url_n_params[0];
	    var params = url_n_params[1].split('&');
	    for (var i = 0; i < params.length; i++) {
	        if(params_to_clean.indexOf(params[i].split("=")[0]) == -1)
        	    required_params[required_params.length] = params[i];    
	    }
	    
	    if(required_params.length > 0)
	        var finalUrl = base_url + "?" + required_params.join("&");
	    else
	        var finalUrl = base_url
		return finalUrl;
	}
	return toReturn;
}
