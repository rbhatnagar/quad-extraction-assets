function filterURL(url){
    var required_elem = ["cid"];
    var final_list=[]
	if(url.indexOf("?") > -1)
	{
		var mainUrl = url.split("?");
		var res =  mainUrl[1].split("&");
		for (var i in res) {
        	if(required_elem.indexOf(res[i].split("=")[0]) != -1)
        	{
        		final_list[final_list.length] = res[i];
        	}
    	}
    	url = mainUrl[0] + "?" + final_list.join("&");
	}
	return url;
}
