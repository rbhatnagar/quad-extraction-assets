function(url) {
	var uniqueAttrs = [];
    var cleansedURL = url.split("&")[0].split("/").filter(function(s){ return s!="";});
    if(cleansedURL.length > 1) {
        var productId = cleansedURL[cleansedURL.length - 1].split("=");
        if(productId.length > 1) {
            uniqueAttrs.push(productId[1]);
        }
    }
    if(uniqueAttrs.length == 0) {
    	return url;
    } else {
    	if(uniqueAttrs[0].indexOf("cat") != -1) {
    		return "http://www.jcpenney.com/cat.jump?id=" + uniqueAttrs[0]; 
    	} else {
    		return url;
    	}
    }
}