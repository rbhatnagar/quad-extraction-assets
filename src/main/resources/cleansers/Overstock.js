function(url) {
	var index = url.indexOf("?sort");
	if(index >= 0) {
		return url.substring(0, index);
	}
	return url;
}