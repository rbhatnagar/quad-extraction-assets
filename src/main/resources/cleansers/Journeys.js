function clean(url) {
    var required_params = []
	var toReturn = url;
	// page_number, gender, size, brand, color and age-group
	var params_to_clean = ['p', 'g', 's', 'b', 'l', 'ag'];
	var index = url.indexOf("?");
	if(index >= 0){
	    var url_n_params = url.split('?');
	    var base_url = url_n_params[0];
	    var params = url_n_params[1].split('&');
	    for (var i in params) {
	        if(params_to_clean.indexOf(params[i].split("=")[0]) == -1)
        	    required_params[required_params.length] = params[i];    
	    }
	    
	    var finalUrl = base_url + "?" + required_params.join("&");
		return finalUrl;
	}
	return toReturn;
}