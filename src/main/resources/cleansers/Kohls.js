function(url) {
	var cleanser = url.split("?");
	if(cleanser.length > 1) {
		var parameter = cleanser[1].split("&");
		if(parameter.length == 1) {
			return cleanser[0] + parameter[0];
		} else {
			if((parameter[0].indexOf("CN=") != -1) && (parameter[1].indexOf("N=") != -1)) {
				return cleanser[0] + "?" + parameter[1];
			} else {
				return cleanser[0] + "?" + parameter[0];
			}
		}
	}
	return url;
}