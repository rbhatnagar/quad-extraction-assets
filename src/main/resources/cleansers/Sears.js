function(url) {
	var cleanser = url.split("?");
    if (cleanser.length > 1) {
    	return cleanser[0];
    }
    return url;
}