function filterURL(url)
{
	var uniqueAttrs = [];
	url = url.split("?")[0].split("//")[1];
	if(url.indexOf("/") > -1)
	{
		var res = url.split("/"); 
		if(res.length>2)
		{
			uniqueAttrs.push(res[2]);
			return uniqueAttrs;
		}
		else
		{
			uniqueAttrs.push(res[1]);
			return uniqueAttrs;
		}
	}
	else
	{
		return uniqueAttrs;
	}
	
}
