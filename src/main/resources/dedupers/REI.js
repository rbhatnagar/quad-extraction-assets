function filterURL(url)
{
	var uniqueAttrs = [];
	if(url.indexOf("?") > -1 && url.indexOf("//") > -1)
	{
		url = url.split("?")[0].split("//")[1];
	}
	else if(url.indexOf("//") > -1)
	{
		url = url.split("//")[1];
	}
	if(url.indexOf("/") > -1)
	{
		var res = url.split("/"); 
		if(res.length>2)
		{
			uniqueAttrs.push(res[2]);
			return uniqueAttrs;
		}
	}
	else
	{
		return uniqueAttrs;
	}
	
}
