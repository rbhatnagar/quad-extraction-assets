function filterURL(url) {
	url = url.split("?")[0];
    var uniqueAttrs = [];
    var split1 = url.split("/");
    var split = split1.filter(function(s){ return s!="";});
    if(split.length > 4)
    {
        id = split[split.length-1]
        if(id.indexOf("R-US_")>-1)
        {    
            id = id.split("R-US_")[1];
        }
        uniqueAttrs.push(id);
    }
    return uniqueAttrs;
}