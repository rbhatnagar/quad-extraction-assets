function filterUrl(url){
    var uniqueAttrs = [];
    
    if(url.indexOf("?") >= 0)
        url = url.split('?')[0]
    
    if(url.indexOf("/") >= 0){
        var unique_id = url.split('/').pop()
        var array = unique_id.split('.');
        array.pop();
        uniqueAttrs = array
    }
        
    return uniqueAttrs
}