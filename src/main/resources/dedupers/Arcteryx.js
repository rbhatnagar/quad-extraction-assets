function filterUrl(url){
    var uniqueAttrs = [];
    var index = url.indexOf("?");
    if(index >= 0){
        var params = url.split('?')[1].split('&');
        
        for (var i in params){
            if(params[i].split("=")[0] == 'model'){
                uniqueAttrs.push(params[i].split('=')[1]);
                break;
            }
        }
    }
    return uniqueAttrs
}