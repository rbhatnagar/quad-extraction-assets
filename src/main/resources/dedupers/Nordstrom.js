function(url) {
    var uniqueAttrs = [];
    var split = url.split("?");
    if(split.length > 0) {
    	var deduper = split[0].split("/").filter(function(s){ return s!="";});
    	if(deduper.length == 5) {
    		uniqueAttrs.push(deduper[4]);
    	}
    }
    return uniqueAttrs;
}