function(url) {
    var uniqueAttrs = [];
    var split = url.split("?");
    if(split.length == 2) {
    	var deduper = split[1].split("&");
    	for (i =0;i<deduper.length;i++){
    		if (deduper[i].indexOf("productId")>-1){
    			uniqueAttrs.push(deduper[i].split("=")[1]);
    			break;
    		}
    	}
    }
    return uniqueAttrs;
}