function(url) {
    var uniqueAttrs = [];
    var split1 = url.split("/");
    var split = split1.filter(function(s){ return s!="";});
    if(split.length < 4) {
    	return uniqueAttrs;
    }
    if ("p" != split[2]) {
    	return uniqueAttrs;
    }
    var r = split[3];
    var parts = r.split("-");
	var idString = parts[parts.length - 1];
    if(!isNaN(idString)) {
   		uniqueAttrs.push(idString);
    }
    return uniqueAttrs;
}