function filterURL(url) {
    //Eg : http://www.patagonia.com/us/product/womens-long-sleeved-capilene-daily-t-shirt?p=45265-0
    //Eg : http://www.patagonia.com/us/product/mens-capilene-midweight-crew?p=44425-0
    var uniqueAttrs = [];
    var split1 = url.split("/");
    var split = split1.filter(function(s){ return s!="";});
    if(split.length < 5) {
      return uniqueAttrs;
    }
    var productDesc = split[4].split("?");
    if (productDesc.length >1){
    	uniqueAttrs.push(productDesc[1].split("&")[0].replace("p=","").split("-")[0]);
    }
    else
    {
        uniqueAttrs.push(productDesc[0]);
    }
    return uniqueAttrs;
}