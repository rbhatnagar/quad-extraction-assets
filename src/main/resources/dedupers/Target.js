function(url) {
    var uniqueAttrs = [];
    var split1 = url.split("/");
    var split = split1.filter(function(s){ return s!="";});
    if(split.length < 3) {
      return uniqueAttrs;
    }
    if("p" != split[2].toLowerCase()) {
        return uniqueAttrs;
    }
    uniqueAttrs.push(split[3]);
    uniqueAttrs.push(split[5].substring(0, split[5].indexOf('#')));
    return uniqueAttrs;
}