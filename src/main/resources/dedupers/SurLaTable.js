function(url) {
    var uniqueAttrs = [];
    var split1 = url.split("/");
    var split = split1.filter(function(s){ return s!="";});
    if(split.length < 4) {
      return uniqueAttrs;
    }
    if("product" != split[2].toLowerCase()) {
    	return uniqueAttrs;
    }
    uniqueAttrs.push(split[3]);
    if(split[4].indexOf("?") != -1) {
    	uniqueAttrs.push(split[4].substring(0, split[4].indexOf('?')));
    } else if(split[4].indexOf(";") != -1) {
    	uniqueAttrs.push(split[4].substring(0, split[4].indexOf(";")));
    } else {
    	uniqueAttrs.push(split[4]);
    }
    return uniqueAttrs;
}