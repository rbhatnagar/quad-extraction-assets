function(url) {
    var uniqueAttrs = [];
    var cleansedURL = url.split("&")[0].split("/").filter(function(s){ return s!="";});
    if(cleansedURL.length > 1) {
        var productId = cleansedURL[cleansedURL.length - 1].split("=");
        if(productId.length > 1) {
            uniqueAttrs.push(productId[1]);
        }
    }
    return uniqueAttrs;
}