function filterURL(url)
{
	var uniqueAttrs = [];
	if(url.indexOf("?") > -1 && url.indexOf("//") > -1)
	{
		url = url.split("?")[0].split("//")[1];
	}
	else if(url.indexOf("//") > -1)
	{
		url = url.split("//")[1];
	}
	if(url.indexOf("/") > -1)
	{
		var res = url.split("/"); 
		if(res.length>1)
		{
			uniqueAttrs.push(res[3]);
			return uniqueAttrs;
		}
	}
	else
	{
		return uniqueAttrs;
	}
	
}